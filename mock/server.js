let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let path = require('path');

// let debug = require('debug')('my-application'); // debug模块

// app.set('port', process.env.PORT || 1111); // 设定监听端口

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//用户权限
let user = require('./user');
user(app);

//首页
let home = require('./home');
home(app);

//个人中心
let personcenter = require('./personcenter')
personcenter(app);

//用户管理
let userManagement = require('./userManagement')
userManagement(app);

app.listen(1111,() =>{
  console.log("mock start, 1111 is ports")
});

//启动监听
// var server = app.listen(app.get('port'), function() {
  // debug('Express server listening on port ' + server.address().port);
// });
