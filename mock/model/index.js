module.exports = function (app){

    let type = require('./type.js')
    app.get('/api/model/type',(req, res) => {
      res.send(type);
    })
    
    app.post('/api/model/tableData',(req, res) => {
      let tableData = require('./tableData.js')
      let page = req.body.page
      let pageList = req.body.pageList || 10
      tableData = tableData.data
      let data = {}
      data.page = page
      data.data = tableData.slice(page, page+pageList)
      data.records = 102
      res.send(data);
    })
}
