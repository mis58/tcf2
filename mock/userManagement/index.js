module.exports = function (app){

    //组织架构
    let orgList = require('./data/org-query.js')
    app.get('/api/user/org/query',(req, res) => {
      res.send(orgList);
    })

    //新建组织架构
    let orgSave = require('./data/org-save.js')
    app.post('/api/user/org/save',(req, res) => {
      res.send(orgSave);
    })

    //权限角色
    let roleList = require('./data/roleList.js')
    app.get('/api/userManagement/role/roleList',(req, res) => {
      res.send(roleList);
    })

    //角色列表
    app.post('/api/userManagement/role/roleTable',(req, res) => {
        let data = require(`./data/roleTable.js`)
        res.send(data);
    })

    //用户列表
    app.post('/api/user/query',(req, res) => {
        let data = require(`./data/query.js`)
        res.send(data);
    })

    //重置密码
    app.post('/api/user/resetPassWord',(req, res) => {
        let data = require(`./data/resetPassWord.js`)
        res.send(data);
    })

    //新增用户
    app.post('/api/user/cu',(req, res) => {
        res.send("succ");
    })

    //tree
    app.post('/api/user/org/getRootBus',(req, res) => {
      let data = require(`./data/getRootBus.js`)
        res.send(data);
    })

    //tree-sub
    app.get('/api/user/org/getSubBus',(req, res) => {
      let data = require(`./data/getSubBus.js`)
        res.send(data);
    })

    //tree-add
    app.get('/api/user/org/addBus',(req, res) => {
      let data = require(`./data/addBus.js`)
        res.send(data);
    })

    //tree-updatename
    app.get('/api/user/org/updateBusName',(req, res) => {
      let data = require(`./data/updateBusName.js`)
        res.send(data);
    })

    //tree-updateorder
    app.get('/api/user/org/updateBusOrder',(req, res) => {
      let data = require(`./data/updateBusOrder.js`)
        res.send(data);
    })

    //tree-del
    app.get('/api/user/org/delBus',(req, res) => {
      let data = require(`./data/delBus.js`)
        res.send(data);
    })


}
