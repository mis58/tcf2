module.exports = {
  data:[{
      value: 'BusinessClerk',
      label: '业务员'
    }, {
      value: 'DocumentClerk',
      label: '单证员'
    }, {
      value: 'RiskControl',
      label: '风控员'
    }, {
      value: 'BusinessLeader',
      label: '业务领导'
    }, {
      value: 'DocumentLeader',
      label: '单证领导'
    }, {
      value: 'Finance',
      label: '财务'
    }]
};
