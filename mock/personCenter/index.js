
module.exports = function (app){

  //获取 用户信息
  let infoObj = require('./data/info.js');
  app.get('/api/user/info',(req, res)=>{
    res.send(infoObj)
  });

  //修改用户信息
  app.post('/api/user/modifyUserInfo',(req, res)=>{
    let modifyMsg = require('./data/modify.js');
    res.send(modifyMsg)
  });

  //修改密码
  app.post('/api/user/modifyPassWord',(req, res)=>{
    let modifyMsg = require('./data/modify.js');
    res.send(modifyMsg)
  });


};
