/* function: 数字金额转换为大写中文
* */
function digitUppercase(n) {
  var fraction = ['角', '分'];
  var digit = [
    '零',
    '壹',
    '贰',
    '叁',
    '肆',
    '伍',
    '陆',
    '柒',
    '捌',
    '玖'
  ];
  var unit = [
    [
      '元', '万', '亿'
    ],
    [
      '', '拾', '佰', '仟'
    ]
  ];
  var head = n < 0
    ? '欠'
    : '';
  n = Math.abs(n);
  var s = '';
  for (var i = 0; i < fraction.length; i++) {
    s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');
  }
  s = s || '整';
  n = Math.floor(n);
  for (var i = 0; i < unit[0].length && n > 0; i++) {
    var p = '';
    for (var j = 0; j < unit[1].length && n > 0; j++) {
      p = digit[n % 10] + unit[1][j] + p;
      n = Math.floor(n / 10);
    }
    s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
  }
  return head + s.replace(/(零.)*零元/, '元').replace(/(零.)+/g, '零').replace(/^整$/, '零元整');
};

function englishmoney(number) {

  var arr1 = new Array("", " thousand", " million", " billion"),
    arr2 = new Array("zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"),
    arr3 = new Array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"),
    arr4 = new Array("ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen");

  function intergernumber(a) {
    var b = a.length,
      f,
      h = 0,
      g = "",
      e = Math.ceil(b / 3),
      k = b - e * 3;
    g = "";
    for (f = k; f < b; f += 3) {
      ++h;
      var num3 = f >= 0
        ? a.substring(f, f + 3)
        : a.substring(0, k + 3);
      var strEng = English(num3);
      if (strEng != "") {
        if (g != "")
          g += ",";
        g += English(num3) + arr1[e - h]
      }
    }
    return g.toUpperCase();
  }

  function decimalnumber2 (a) {

    var b = a.length,
      f,
      h = 0,
      g = "",
      e = Math.ceil(b / 3),
      k = b - e * 3;
    g = "";
    for (f = k; f < b; f += 3) {
      ++h;
      var num3 = f >= 0
        ? a.substring(f, f + 3)
        : a.substring(0, k + 3);
      var strEng = English(num3);
      if (strEng != "") {
        if (g != "")
          g += ",";
        g += English(num3) + arr1[e - h]
      }
    }
    return "CENTS " + g.toUpperCase();
  }

   function decimalpoint (a) {

    var b = a.length,
      f,
      h = 0,
      g = "",
      e = Math.ceil(b / 3),
      k = b - e * 3;
    g = "";
    for (f = k; f < b; f += 3) {
      ++h;
      var num3 = f >= 0
        ? a.substring(f, f + 3)
        : a.substring(0, k + 3);
      var strEng = English(num3);
      if (strEng != "") {
        if (g != "")
          g += ",";
        g += English(num3) + arr1[e - h]
      }
    }
    return "POINT " + g.toUpperCase() + " ONLY";
  }

  function fraction (a) {

    var b = a.length,
      f,
      h = 0,
      g = "",
      e = Math.ceil(b / 3),
      k = b - e * 3;
    g = "";
    for (f = k; f < b; f += 3) {
      ++h;
      var num3 = f >= 0
        ? a.substring(f, f + 3)
        : a.substring(0, k + 3);
      var strEng = English(num3);
      if (strEng != "") {
        if (g != "")
          g += ",";
        g += English(num3) + arr1[e - h]
      }
    }
    return g.toUpperCase();
  }

  function numbertoChinayuan (a) {
    var b = 9.999999999999E10,
      f = "零",
      h = "壹",
      g = "贰",
      e = "叁",
      k = "肆",
      p = "伍",
      q = "陆",
      r = "柒",
      s = "捌",
      t = "玖",
      l = "拾",
      d = "佰",
      i = "仟",
      m = "万",
      j = "亿",
      o = "元",
      c = "角",
      n = "分",
      v = "整";

    a = a.toString();
    if (a == "") {
      alert("Use only numbers!");
      return "";
    }
    if (a.match(/[^,.\d]/) != null) {
      alert("Use only numbers!");
      return "";
    }
    if (a.match(/^((\d{1,3}(,\d{3})*(.((\d{3},)*\d{1,3}))?)|(\d+(.\d+)?))$/) == null) {
      alert("Use only numbers!");
      return "";
    }
    a = a.replace(/,/g, "");
    a = a.replace(/^0+/, "");
    if (Number(a) > b) {
      alert("Maximum number is 99999999999.99!");
      return "";
    }
    b = a.split(".");
    if (b.length > 1) {
      a = b[0];
      b = b[1];
      b = b.substr(0, 2)
    } else {
      a = b[0];
      b = "";
    }
    h = new Array(f, h, g, e, k, p, q, r, s, t);
    l = new Array("", l, d, i);
    m = new Array("", m, j);
    n = new Array(c, n);
    c = "";
    if (Number(a) > 0) {
      for (d = j = 0; d < a.length; d++) {
        e = a.length - d - 1;
        i = a.substr(d, 1);
        g = e / 4;
        e = e % 4;
        if (i == "0")
          j++;
        else {
          if (j > 0)
            c += h[0];
          j = 0;
          c += h[Number(i)] + l[e];
        }
        if (e == 0 && j < 4)
          c += m[g];
        }
      c += o;
    }
    if (b != "")
      for (d = 0; d < b.length; d++) {
        i = b.substr(d, 1);
        if (i != "0")
          c += h[Number(i)] + n[d];
        }
      if (c == "")
        c = f + o;
  if (b.length < 2)
      c += v;
    return c = "Say ChineseYuan: " + c
  }

   function numbertotext (a) {
    var b = a.length,
      f,
      h = 0,
      g = "",
      e = Math.ceil(b / 3),
      k = b - e * 3;
    g = "";
    for (f = k; f < b; f += 3) {
      ++h;
      var num3 = f >= 0
        ? a.substring(f, f + 3)
        : a.substring(0, k + 3);
      var strEng = English(num3);
      if (strEng != "") {
        if (g != "")
          g += ",";
        g += English(num3) + arr1[e - h]
      }
    }
    // return "SAY US DOLLARS " + g.toUpperCase() + " ONLY";
    return g.toUpperCase();
  }

  function English (a) {
    var strRet = "";
    if (a.length == 3 && a.substr(0, 3) != "000") {
      if (a.substr(0, 1) != "0") {
        strRet += arr3[a.substr(0, 1)] + " hundred";
        if (a.substr(1, 2) != "00")
          strRet += " and "
      }
      a = a.substring(1);
    }
    if (a.length == 2)
      if (a.substr(0, 1) == "0")
        a = a.substring(1);
      else if (a.substr(0, 1) == "1")
        strRet += arr4[a.substr(1, 2)];
      else {
        strRet += arr2[a.substr(0, 1)];
        if (a.substr(1, 1) != "0")
          strRet += "-";
        a = a.substring(1)
      }
    if (a.length == 1 && a.substr(0, 1) != "0")
      strRet += arr3[a.substr(0, 1)];
    return strRet;
  };

  // var findObj = findObj(a, b) {
  //   var c, d;
  //   b || (b = document);
  //   if ((c = a.indexOf("?")) > 0 && parent.frames.length) {
  //     b = parent.frames[a.substring(c + 1)].document;
  //     a = a.substring(0, c);
  //   }
  //   if (!(d = b[a]) && b.all)
  //     d = b.all[a];
  //   for (c = 0; !d && c < b.forms.length; c++)
  //     d = b.forms[c][a];
  //   for (c = 0; !d && b.layers && c < b.layers.length; c++)
  //     d = findObj(a, b.layers[c].document);
  //   if (!d && document.getElementById)
  //     d = document.getElementById(a);
  //   return d;
  // }
  number = '' + number;

  if (number.indexOf(".") != -1) {

    var integer = number.split(".")[0];

    var decimal = number.split(".")[1];

    var decimalword,
      str;

    var interword = intergernumber(integer);

    if (decimal.length > 2) {
      // alert("The figure is account to two decimal places.eg：8765.55")
    } else {

      var decimalword = decimalnumber2(decimal);
      //point format
      var decimaltopoint = decimalpoint(decimal);
      //fraction foramt
      var decimalfraction = fraction(decimal);
    }
    str = interword + " AND " + decimalword;
    return str;

  } else {
    return numbertotext(number);
  }
}


/**
 * 对数字保留指定的位数, 四舍五入
 * @param {number || string} num 转换的数字 
 * @param {number} bits  保留的位数
 */
const toFixedRound = function(num, bits){
  if (!num) return "0.00";

   bits = bits || 2;
   let str = num.toString().split(".");
   if (str.length == 2) {
     let tail = str[1];
     let len = tail.length;
     if (len < bits){
       let lack = bits  - len;
       return num + Array(lack).fill('0').join('');
     } else if (len == bits){
       return num.toString();
     } else if (len > bits){
       let last = Number(tail[bits - 1]);
       let afterLast = Number(tail[bits]);
       if (afterLast >= 5){
          last += 1;
          return str[0] + '.' + tail.slice(0, bits - 1) + last;
       } else {
         return str[0] + '.' + tail.slice(0, bits);
       }
     }

   } else {
     return num + '.' + Array(bits).fill('0').join('') ;
   }
}



/**
 * 对数字保留指定的位数, 不需要四舍五入
 * @param {number || string} num 转换的数字 
 * @param {number} bits  保留的位数
 */
const toFixed = function(num, bits){
  if (!num) return "0.00";
  bits = bits || 2;
  let str = num.toString().split(".");
  if (str.length == 2) {
    let tail = str[1];
    let len = tail.length;
    if (len < bits){
      let lack = bits  - len;
      return num + Array(lack).fill('0').join('');
    } else if (len == bits){
      return num.toString();
    } else if (len > bits){
      return str[0] + '.' + str[1].slice(0,2);
    }

  } else {
    return num + '.' + Array(bits).fill('0').join('') ;
  }
}

export {
  digitUppercase,
  englishmoney,
  toFixed,
  toFixedRound
}
