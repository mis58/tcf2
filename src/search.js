import Vue from 'vue'
import App2 from './App2'

import ElementUI from 'element-ui'
import _ from 'lodash'

Vue.use(ElementUI)

new Vue({
  el: '#app',
  template: `<App2/>`,
  components: {App2}
})
