import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

//修改loading 状态
const UPDATALOADINGSTATE = "updateLoadingState"

const loading = {
  state:{
    isLoading:false
  },
  mutations:{
    UPDATALOADINGSTATE (state, flag) {
      state.isLoading = flag
    }
  },
  actions:{
    onLoading ({commit, state},flag) {
      commit("UPDATALOADINGSTATE", flag)
    }
  },
  getters:{
    stateLoading: state => state.isLoading
  },
}

export default loading
