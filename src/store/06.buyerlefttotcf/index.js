import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const cdStore = {
  namespaced: true,
  state: {
    hasSaved: false,
    attachedFiles: [],
    relationFormInstId: [], //关联订单
    buyerlefttotcf_bindFContracts: [], //绑定的外贸合同
    buyerlefttotcf_bindDContracts: [], //绑定的内贸合同

    bindingFormInstId: '', //绑定的外贸订单的id
    orderCode: '',
    buyerName: '',

    isSalesMan: true,

    'newApprovalName': '',

    //该流程中的所有审批项
    approvalNames: ["业务领导审批尾款收汇通知单", "财务审批尾款收汇通知单"],

    newApproval: {
      "approval_opinion":"",
      "approval_username":"",
      "approval_time":"",
      "approval_div_name": ""
    },

    prevApprovals: [],

    remainApprovals: [],

    //该流程中的所有审批项
    "originApprovals":[{
      "approval_opinion":"",
      "approval_username":"",
      "approval_time":"",
      "approval_div_name":"业务领导审批尾款收汇通知单",
      "rw": 'R',
      item: 'item_06_01',
    }, {
      "approval_opinion":"",
      "approval_username":"",
      "approval_time":"",
      "approval_div_name":"财务审批尾款收汇通知单",
      "rw": 'R',
      item: 'item_06_02',
    }],
  },
  actions,
  getters,
  mutations
}

export default cdStore
