import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import getters from './getters'
import mutations from './mutations'

import loading from './loading'

import dueout from './dueout'
import businessform  from  './01.businessform'
import buyertotcf from  './02.buyertotcf'
import tcftofactory from  './03.tcftofactory'
import tcflefttofactory from  './04.tcflefttofactory'
import documentprocess  from  './05.documentprocess'
import buyerlefttotcf from './06.buyerlefttotcf'


Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
      loading,
      dueout,
      businessform,
      buyertotcf,
      tcftofactory,
      tcflefttofactory,
      documentprocess,
      buyerlefttotcf
    },
    state() {
      return {

      }
  },
  getters,
  mutations,
  actions
})

export default store
