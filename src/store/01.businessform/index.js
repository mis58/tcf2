import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const cdStore = {
  namespaced: true,
  state() {

    return {
      //业务提单中所有表都会显示的订单号
      businessform_no: '',

      hasSaved: false,

      //业务员填写流程
      "isSalesMan": true,

      'newApprovalName': '',

      "approvalNames": [
        "单证员审批合同信息", "业务领导审批合同信息", "风控员审批合同信息", "财务审批合同信息"
      ],

      newApproval: {
        "approval_opinion": "",
        "approval_username": "",
        "approval_time": "",
        "approval_div_name": ""
      },

      prevApprovals: [],

      remainApprovals: [],

      //该流程中的所有审批项
      "originApprovals": [
        {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "单证员审批合同信息",
          rw: 'R',
          item: 'item_01_01'
        }, {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "业务领导审批合同信息",
          rw: 'R',
          item: 'item_01_02'
        }, {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "风控员审批合同信息",
          rw: 'R',
          item: 'item_01_03'
        }, {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "财务审批合同信息",
          rw: 'R',
          item: 'item_01_04'
        }
      ],

      //业务审核单
      businessform_businesscheckform: {
        attachedFiles: [], //票据附件
        bb_table1: [
          {
            bb_productName: '', //产品名称
            bb_productFunction: '', //产品用途
            bb_productUse: '', //产品用途
            bb_hasBrand: '', //产品是否有品牌
            bb_productCode: '', //产品HS编码
            bb_exportCountry: '', //出口国家
            bb_qualification: '', //工厂资质
            bb_needCheck: '', //是否需要商检
            bb_otherNeed: '', //其他需求
          }
        ]
      },

      //外贸合同
      businessform_foreigntradecontract: {
        attachedFiles: [], //票据附件
        bf_orderNumber: '', //订单编号
        bf_orderDate: '', //订单日期
        bf_tradeTerm: '', //成交方式
        bf_shippingMarks: '', //唛头
        bf_delivery: '', //交货期
        bf_portOfLoad: '', //装运港
        bf_portOfDestination: '', //目的港
        bf_shipment: '', //运输
        bf_downPaymentRate: '', //定金比例
        bf_downPayment: '', //定金
        bf_symbol: [
          '√', '×'
        ],

        bf_buy_nameOptions: [
          'xxx', 'yyy'
        ],

        bf_currencyValue: '人民币',

        //单据要求
        bf_commercialInvoice: '', //商业发票
        bf_fullForm: '', //全套提单
        bf_airParcel: '', //空运
        bf_fullInsurance: '', //全套保险单据
        bf_packageList: '', //装箱单
        bf_originCertificate: '', //原产地证


        //份数
        bf_commercialInvoice_num: 0,
        bf_fullInsurance_num: 0, 
        bf_packageList_num: 0,
        bf_originCertificate_num: 0,

        //copies
        bf_commercialInvoice_copy_num: 0,
        bf_fullInsurance_copy_num: 0, 
        bf_packageList_copy_num: 0,
        bf_originCertificate_copy_num: 0,


        //买家信息
        bf_buy_name: '', //买方
        bf_buy_address: '', //买方地址
        bf_buy_tel: '', //电话
        bf_buy_email: '', //邮箱
        bf_buy_fax: '', //传真


        bf_depositMoney: 0, //定金
        bf_balanceMoney: 0, //尾款
        
        //有一个表格
        bf_table1: [
          {
            bf_table1_product: '', //产品
            bf_table1_description: '', //产品描述
            bf_table1_price: '', //价格
            bf_table1_lineAmount: '', //一行的金额,
            bf_table1_quantity: '' //一行数量
          }, {
            bf_table1_product: '', //产品
            bf_table1_description: '', //产品描述
            bf_table1_price: '', //价格
            bf_table1_lineAmount: '', //一行的金额,
            bf_table1_quantity: '' //一行数量
          }, {
            bf_table1_product: '', //产品
            bf_table1_description: '', //产品描述
            bf_table1_price: '', //价格
            bf_table1_lineAmount: '', //一行的金额,
            bf_table1_quantity: '' //一行数量
          }
        ]
      },

      //PI
      businessform_pi: {
        attachedFiles: [], //票据附件

        bp_countryOrigin: '中国China',
        bp_benCompany: 'TIAN JIN FINANCIAL TRADE LINK IMPORT AND EXPORT CO.,LTD	',
        bp_AccountNo: '1000000010120100506972',
        bp_CompanyAddress: 'NO.88-2-2205,HUANHE SOUTH ROAD,CHINA(TIANJIN)PILOT FREE TRADE ZONE TIANJIN AIRPORT ECONOMIC AREA),TIANJIN,CHINA	',
        bp_bank: 'CHINA ZHESHANG BANK BEIJING BRANCH', 	
        bp_bankAddress: 'NO.1 FINANCIAL STEREET,XICHENG DISTRICT,BEIJING,CHINA',
        bp_swiftCode: 'ZJCBCN2N', 	
        bp_countryAndManu: 'China', //原产地
        bp_packing: 'To be packed in standard packing',
        bp_insurance: 'Without Insurance of the shipment by sea',



        //pi 头部信息
        bp_company: '', // 公司
        bp_no: '', //编号
        bp_address: '', //地址
        bp_date: '', //日期


        bp_portOfLoading: '', //出港地址
        bp_portOfDestination: '', //目的港
        bp_paymentTerm: '', //付款方式,
        bp_tradeTerm: 'EXW', //成交方式

        bp_currencyValue: '人民币',

        bp_depositMoney: 0, //定金
        bp_balanceMoney: 0, //

        //一个表格
        bp_table1: [
          {
            bp_table1_products: '',
            bp_table1_hscode: '',
            bp_table1_description: '',
            bp_table1_quantity: '',
            bp_table1_price: '',
            bp_table1_lineAmount: '',
            bp_table1_totalAmount: ''
          }
        ]
      },

      //内贸合同
      businessform_domestictradecontract: [
        {
          attachedFiles: [], //票据附件
          //合同头部信息
          bd_no: '', //编号
          bd_date: '', //日期
          bd_seller: '', //卖方
          bd_sellerAddress: '', //卖方地址
          bd_sellerTel: '', //卖方电话
          bd_sellerFax: '', //卖方传真
          bd_sellerEmail: '', //卖方邮箱

          bd_sellerOptions: [
            'xxx', 'yyy'
          ],

          bd_currencyValue: '人民币',

          //卖方开户行信息，英文版
          bd_en_name: '',
          bd_en_bankOfDeposit: '',
          bd_en_account: '',

          //卖方开户行信息，中文版
          bd_ch_name: '',
          bd_ch_bankOfDeposit: '',
          bd_ch_account: '',


          bd_depositMoney: 0, //定金
          bd_balanceMoney: 0, //尾款

          bd_table1: [
            {
              bd_table1_products: '',
              bd_table1_description: '',
              bd_table1_quantity: '',
              bd_table1_price: '',
              bd_table1_lineAmount: '',
              bd_table1_totalAmount: ''
            }, {
              bd_table1_products: '',
              bd_table1_description: '',
              bd_table1_quantity: '',
              bd_table1_price: '',
              bd_table1_lineAmount: '',
              bd_table1_totalAmount: ''
            }
          ]
        }
      ],

      //利润测算
      businessform_profitcalculation: {
        attachedFiles: [], //票据附件
        bpc_no: '', //编号
        bpc_exchangeRate: '', //汇率
        bpc_logistics: '', //物流
        bpc_agencyFee: '', //中介费
        bpc_other: '', //其他


        bpc_currencyValue: '人民币',

        //订单情况表
        bpc_table1: [
          {
            bpc_table1_product: '',
            bpc_table1_hscode: '',
            bpc_table1_quantity: '',
            bpc_table1_price: '',
            bpc_table1_lineAmount: '',
            bpc_table1_taxRebates: '',
            bpc_table1_lineTaxRebatesAmount: '',
            bpc_table1_amounts: '',
            bpc_table1_taxRebatesAmounts: ''
          }, {
            bpc_table1_product: '',
            bpc_table1_hscode: '',
            bpc_table1_quantity: '',
            bpc_table1_price: '',
            bpc_table1_lineAmount: '',
            bpc_table1_taxRebates: '',
            bpc_table1_lineTaxRebatesAmount: '',
            bpc_table1_amounts: '',
            bpc_table1_taxRebatesAmounts: ''
          }
        ],

        //服务收费情况表
        bpc_table2: [
          {
            bpc_table2_first: true,
            bpc_table2_foreignTradeContractAmount: '', //外贸合同金额
            bpc_table2_securityDeposit: '', //保证金
            bpc_table2_foreignTradeCreditAmount: '', //外贸赊销款
            bpc_table2_domesticTradeContractAmount: '', //内贸合同金额
            bpc_table2_factoryDownPayment: '', //工厂定金
            bpc_table2_factoryRetainage: '', //工厂尾款
            bpc_table2_creditSalesAmount: '', //赊销金额
            bpc_table2_agentRefund: '', //代理退税费率!!
            bpc_table2_creditExportServices: '', // 赊销出口服务费率!!
            bpc_table2_financingService: '' //融资服务费率!!
          }, {
            bpc_table2_foreignTradeContractAmount: '', //外贸合同金额
            bpc_table2_securityDeposit: '', //保证金
            bpc_table2_foreignTradeCreditAmount: '', // 外贸赊销款
            bpc_table2_domesticTradeContractAmount: '', //内贸合同金额
            bpc_table2_factoryDownPayment: '', //工厂定金
            bpc_table2_factoryRetainage: '', //工厂尾款
            bpc_table2_creditSalesAmount: '', // 赊销金额
            bpc_table2_agentRefund: '', //代理退税费
            bpc_table2_creditExportServices: '', //赊销出口服务费
            bpc_table2_financingService: '' //融资服务费
          }, {
            bpc_table2_domesticTradeContractAmount: '', //内贸合同金额
          }
        ]
      },

      relationFormInstId: []
    }
  },
  actions,
  getters,
  mutations
}

export default cdStore
