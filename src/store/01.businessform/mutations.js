import * as Types from './mutations-types.js'


let  defaultState = {};

export const mutations = {
  [Types.SAVE_FORM] (state, value) {
    //保存 businessform中所有表的数据
    let temp = _.cloneDeep(value);
    for (var prop in temp) {
      state[prop] = temp[prop];
    }
  },

  [Types.ADD_DTC] (state, value){

    //添加新的内贸合同时，需要保存已经填好的内贸合同
     state.businessform_domestictradecontract = _.cloneDeep(value)

    //添加内贸
    let current = state.businessform_domestictradecontract;
    current.push(_.cloneDeep(current[0]));

  },

  [Types.DEL_DTC] (state, value){
    // 移除内贸
    // let current = state.businessform_domestictradecontract;
    // current.splice(index, 1);
     state.businessform_domestictradecontract = _.cloneDeep(value)
  },

  //保存初始的state
  [Types.SAVE_INITIAL_DATA] (state, value){
    let temp = _.cloneDeep(state);
    for (var prop in temp) {
      defaultState[prop] = temp[prop];
    }

    // console.log("第一次保存的 defaultState = ", defaultState);
  },

  //重置操作
  [Types.RESET_INITIAL_DATA] (state, value){

    // console.log("重置时的 defaultState = ", defaultState);

    let temp = _.cloneDeep(defaultState);
    for (var prop in temp) {
      state[prop] = temp[prop];
    }
    // console.log("mutations中重置完成后的值：", state);
  },

  [Types.SET_SAVE](state, value){
    state.hasSaved = true;
  }
}

export default mutations
