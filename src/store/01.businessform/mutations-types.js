 export const SAVE_FORM = 'SAVE_FORM' ;  //保存按钮
 export const SEND_FORM = 'SEND_FORM' ;  //发送按钮
 export const ADD_DTC = 'ADD_DTC'; //添加一个内贸合同
 export const DEL_DTC = 'DEL_DTC'; //删除一个内贸合同
 export const SET_SAVE = 'SET_SAVE'; 

 export const SAVE_INITIAL_DATA = 'SAVE_INITIAL_DATA'; //保存state中的初值(空值)

 export const RESET_INITIAL_DATA = 'RESET_INITIAL_DATA'; //重新初始化state
