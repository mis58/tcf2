import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const cdStore = {
  namespaced: true,
  state: {
    hasSaved: false,
    relationFormInstId: [], //关联订单

    isSalesMan: true,
    'newApprovalName': '',

    //该流程中的所有审批项
    approvalNames: [
      "单证领导审批出厂确认", "风控审批出厂确认", "单证领导审批报关合同", "单证领导审批开票信息", "单证领导审批核销资料"
    ],

   /*
    approvalNames: [
      "单证填报出厂确认","单证领导审批出厂确认", "风控审批出厂确认", "单证填报报关合同",
       "单证领导审批报关合同", "单证填报开票信息","单证领导审批开票信息", "单证填报核销资料", "单证领导审批核销资料"
    ],
    */

    newApproval: {
      "approval_opinion": "",
      "approval_username": "",
      "approval_time": "",
      "approval_div_name": ""
    },

    prevApprovals: [],

    //该流程中的所有审批项
    "originApprovals": [
       {
         "approval_opinion": "",
         "approval_username": "",
         "approval_time": "",
         "approval_div_name": "单证领导审批出厂确认",
         "rw": 'R',
         item: 'item_05_01',
       }, {
         "approval_opinion": "",
         "approval_username": "",
         "approval_time": "",
         "approval_div_name": "风控审批出厂确认",
         "rw": 'R',
         item: 'item_05_02',
       }, {
         "approval_opinion": "",
         "approval_username": "",
         "approval_time": "",
         "approval_div_name": "单证领导审批报关合同",
         "rw": 'R',
         item: 'item_05_03',
       }, {
         "approval_opinion": "",
         "approval_username": "",
         "approval_time": "",
         "approval_div_name": "单证领导审批开票信息",
         "rw": 'R',
         item: 'item_05_04',
       }, {
         "approval_opinion": "",
         "approval_username": "",
         "approval_time": "",
         "approval_div_name": "单证领导审批核销资料",
         "rw": 'R',
         item: 'item_05_05',
       }
     ],


  /*
     "originApprovals": [
       {
         "approval_opinion": "",
         "approval_username": "",
         "approval_time": "",
         "approval_div_name": "单证填报审批出厂确认",
         "rw": 'R',
         item: 'item_05_01',
       },
        {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "单证领导审批出厂确认",
          "rw": 'R',
          item: 'item_05_02',
        }, {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "风控审批出厂确认",
          "rw": 'R',
          item: 'item_05_03',
        }, {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "单证填报审批报关合同",
          "rw": 'R',
          item: 'item_05_04',
        },
        {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "单证领导审批报关合同",
          "rw": 'R',
          item: 'item_05_05',
        },
        {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "单证填报开票信息",
          "rw": 'R',
          item: 'item_05_06',
        }, {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "单证领导审批开票信息",
          "rw": 'R',
          item: 'item_05_07',
        },
        , {
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "单证填报核销资料",
          "rw": 'R',
          item: 'item_05_08',
        },{
          "approval_opinion": "",
          "approval_username": "",
          "approval_time": "",
          "approval_div_name": "单证领导审批核销资料",
          "rw": 'R',
          item: 'item_05_09',
        }
      ],

  */

    //工厂发货
    documentprocess_fd: {

      //发货通知
      documentProcess_fd_notice: {
        attachedFiles: [],

        //绑定的外贸订单id
        bindingFormInstId: '',

        orderCode: '',
        buyerName: '',
        documentprocess_fdn_bindFContracts: [],

        documentprocess_fdn_bindDContracts: [],

        documentprocess_fdn_contracts: '',

        //币种
        documentprocess_fdn_currencyValue: '人民币',

        //发货时间
        documentprocess_fdn_time: '',
        //交易方式
        documentprocess_fdn_tradeTerm: '',

        //海关编码
        documentprocess_fdn_hscode: '',

        //other
        documentprocess_fdn_other: '',

        //工厂联系方式
        documentprocess_fdn_fc_factoryName: '',
        documentprocess_fdn_fc_dff_contacts:[ 
          {
            name: '',
            tel: '',
            email: '',
            qq: '',
            wechat: '',
            factoryName: ''
          }],

        //物流联系方式
        documentprocess_fdn_lcc_logisticName: '',
        documentprocess_fdn_lcc_dfl_contacts: [
          {
            name: '',
            tel: '',
            email: '',
            qq: '',
            wechat: ''
          }
        ],

        //发货明细
        documentprocess_deliDetail: [
          {
            dd_products: '',
            dd_description: '',
            dd_quantity: '',
            dd_price: '',
            dd_lineAmount: ''
          }, {
            dd_products: '',
            dd_description: '',
            dd_quantity: '',
            dd_price: '',
            dd_lineAmount: ''
          }
        ]
      },

      //出厂确认
      documentProcess_fd_confirm: {
        attachedFiles: []
      }

    },

    //单证员报关
    documentprocess_cc: {

      //货运委托书
      documentprocess_cc_letterOfDelivery: {

        attachedFiles: [], //附件
        documentprocess_cc_lod_from: '', //
        documentprocess_cc_lod_to: '',
        documentprocess_cc_lod_fax: '', //货代fax
        documentprocess_cc_lod_transport: '', //运输
        documentprocess_cc_lod_contractNO: '', //合同编号
        documentprocess_cc_lod_consignee: '', //收货人
        documentprocess_cc_lod_caddress: '', //收货地址
        documentprocess_cc_lod_ctel: '', //收货电话
        documentprocess_cc_lod_seafreightCH: '预付', //海洋运费 中文
        documentprocess_cc_lod_seafreightEN: 'PREPAID', //海洋运费 英文
        documentprocess_cc_lod_loadPort: '', //起运港
        documentprocess_cc_lod_destPort: '', //目的港
        documentprocess_cc_lod_containers: '', //集装箱配数
        documentprocess_cc_lod_shipDate: '', //装期
        documentprocess_cc_lod_shipMarks: '', //标记码
        documentprocess_cc_lod_numbers: '', //件数
        documentprocess_cc_lod_products: '', //产品名称
        documentprocess_cc_lod_weight: '', //毛重
        documentprocess_cc_lod_size: '', //尺寸
        documentprocess_cc_lod_tradeTerms: '', //交易方式
        documentprocess_cc_lod_doorToDoor: '', //门对门地址
        documentprocess_cc_lod_entry: '', //货物进仓
        documentprocess_cc_lod_signer: '', //签名
        documentprocess_cc_lod_backAddress: '', //退单地址
        documentprocess_cc_lod_rise: '', //开票抬头
        documentprocess_cc_lod_contact: '', //联系人
        documentprocess_cc_lod_contactTel: '', //联系电话
        documentprocess_cc_lod_contactFax: '', //传真
        documentprocess_cc_lod_date: '', //订单日期
      },

      //报关发票
      documentprocess_cc_customsDeclarationInvoice: {
        attachedFiles: [], //附件
        documentprocess_cc_cdi_toName: '', //买方
        documentprocess_cc_cdi_toAdd: '', //买方地址
        documentprocess_cc_cdi_toTel: '', //买方电话
        documentprocess_cc_cdi_invoiceNO: '', //发票编号
        documentprocess_cc_cdi_invoiceDate: '', //发票日期
        documentprocess_cc_cdi_fromPort: '', //出发港
        documentprocess_cc_cdi_toPort: '', //目的港
        documentprocess_cc_cdi_shipDate: '', //装期
        documentprocess_cc_cdi_transport: '', //运输方式
        documentprocess_cc_cdi_payTerms: 'BY T/T', //付款方式
        documentprocess_cc_cdi_tradeTerms: '', //交易方式
        documentprocess_cc_cdi_currencyValue: '', //币种

        documentprocess_cc_cdi_titleUP: 'Unit Price(USD)',
        documentprocess_cc_cdi_titleAm: 'Amount(USD)',

        documentprocess_cc_cdi_table: [
          {
            documentprocess_cc_cdi_products: '',
            documentprocess_cc_cdi_description: '',
            documentprocess_cc_cdi_quantity: '',
            documentprocess_cc_cdi_price: '',
            documentprocess_cc_cdi_lineAmount: ''
          }, {
            documentprocess_cc_cdi_products: '',
            documentprocess_cc_cdi_description: '',
            documentprocess_cc_cdi_quantity: '',
            documentprocess_cc_cdi_price: '',
            documentprocess_cc_cdi_lineAmount: ''
          }
        ]

      },

      //报关箱单
      documentprocess_cc_customsDeclarationBox: {
        attachedFiles: [],
        documentprocess_cc_cdb_toName: '',
        documentprocess_cc_cdb_toAdd: '',
        documentprocess_cc_cdb_toTel: '',
        documentprocess_cc_cdb_invoiceNO: '',
        documentprocess_cc_cdb_invoiceDate: '',
        documentprocess_cc_cdb_fromPort: '',
        documentprocess_cc_cdb_toPort: '',
        documentprocess_cc_cdb_shipDate: '',
        documentprocess_cc_cdb_transport: '',
        documentprocess_cc_cdb_payTerms: 'BY T/T',
        documentprocess_cc_cdb_tradeTerms: '',
        documentprocess_cc_cdb_table: [
          {
            documentprocess_cc_cdb_products: '', //产品
            documentprocess_cc_cdb_description: '', //描述
            documentprocess_cc_cdb_quantity: 0, //数量
            documentprocess_cc_cdb_packagenumber: 0, //
            documentprocess_cc_cdb_cartons: 0, //编号
            documentprocess_cc_cdb_netWeight: 0, //net weight
            documentprocess_cc_cdb_grossWeight: 0, //gross weight
            documentprocess_cc_cdb_measurement: 0 // measurement
          }, {
            documentprocess_cc_cdb_products: '', //产品
            documentprocess_cc_cdb_description: '', //描述
            documentprocess_cc_cdb_packagenumber: 0, //
            documentprocess_cc_cdb_quantity: 0, //数量
            documentprocess_cc_cdb_cartons: 0, //编号
            documentprocess_cc_cdb_netWeight: 0, //net weight
            documentprocess_cc_cdb_grossWeight: 0, //gross weight
            documentprocess_cc_cdb_measurement: 0 // measurement
          }
        ],

        documentprocess_cc_cdb_totalCartons: 0,
        documentprocess_cc_cdb_totalCartonsUnit: 'PKGS',
        documentprocess_cc_cdb_totalNetWeight: 0,
        documentprocess_cc_cdb_totalGrossWeight: 0,
        documentprocess_cc_cdb_totalVolume: 0,
        documentprocess_cc_cdb_marks: '',
        documentprocess_cc_cdb_shipMarks: ''
      },

      //报关合同委托书
      documentprocess_cc_customsDeclarationContract: {
        attachedFiles: [],
        documentprocess_cc_cdc_contractNO: '', //合同编号
        documentprocess_cc_cdc_contractDate: '', //签订日期
        documentprocess_cc_cdc_buyer: '', //买方
        documentprocess_cc_cdc_buyerAdd: '', //买方地址
        documentprocess_cc_cdc_buyerTel: '', //买方电话
        documentprocess_cc_cdc_description: '', //产品描述
        documentprocess_cc_cdc_quantitys: '', //产品数量
        documentprocess_cc_cdc_quantityUnit: '', //产品数量单位
        documentprocess_cc_cdc_amounts: '', //产品金额
        documentprocess_cc_cdc_amounts_usd: 'USD', //金额单位
        documentprocess_cc_cdc_payTerms: '', //付款方式
        documentprocess_cc_cdc_shipDate: '', //装期
        documentprocess_cc_cdc_loadPort: '', //出发港
        documentprocess_cc_cdc_marks: '', //ship marks
        documentprocess_cc_cdc_destPort: '', //目的港
        documentprocess_cc_cdc_otherTerms: '',//other terms
        documentprocess_cc_cdc_insurance: 'TO BE COVERED BY BUYER ', //insurance
      },

      //报关单
      documentprocess_cc_declarationForm: {
        attachedFiles: [], //附件
        documentprocess_cc_df_enterNO: '', //录入编号
        documentprocess_cc_df_hscode: '', //海关编号
        documentprocess_cc_df_company: '', //单位
        documentprocess_cc_df_exportPort: '', //出口口岸
        documentprocess_cc_df_exportDate: '', //出口日期
        documentprocess_cc_df_applyDate: '', //申请日期
        documentprocess_cc_df_transport: '', //运输方式
        documentprocess_cc_df_transportTool: '', //运输工具
        documentprocess_cc_df_delNO: '', //提运单号
        documentprocess_cc_df_recordNO: '', //备案号
        documentprocess_cc_df_tradeCountry: '', //贸易国
        documentprocess_cc_df_destCountry: '', //运抵国
        documentprocess_cc_df_specPort: '', //指运港
        documentprocess_cc_df_yardGoods: '', //境内货源
        documentprocess_cc_df_license: '', //许可证
        documentprocess_cc_df_tradeTerms: '', //成交方式
        documentprocess_cc_df_freight: '', //运费
        documentprocess_cc_df_premium: '', //保费
        documentprocess_cc_df_otherCharge: '', //杂费
        documentprocess_cc_df_contractNO: '', //合同编号
        documentprocess_cc_df_numbers: '', //件数
        documentprocess_cc_df_cartons: '', //包装种类
        documentprocess_cc_df_grossWeight: '', //毛重
        documentprocess_cc_df_netWeight: '', //净重
        documentprocess_cc_df_containerNO: '', //集装箱号
        documentprocess_cc_df_attachBills: '', //随附单据
        documentprocess_cc_df_manufacturer: '', //生产厂家
        documentprocess_cc_df_marks: '', //标记
        documentprocess_cc_df_table: [
          {
            documentprocess_cc_df_productNO: '', //商品编号
            
            documentprocess_cc_df_productName: '', //商品名称

            documentprocess_cc_df_productDesc: '', //商品描述


            documentprocess_cc_df_quantityAndUnit: [0, '个'], //数量个数 以及 单位

            documentprocess_cc_df_weightAndUnit: [0, 'KGS'], //重量 以及 单位

            // documentprocess_cc_df_prodAndDesc: [
            //   '', ''
            // ], //商品以及描述


            // documentprocess_cc_df_quaAndWeight: [
            //   0, '个', 0, 'KGS'
            // ], //数量，重量单位

            documentprocess_cc_df_destnationContry: '', //最终运抵国
            documentprocess_cc_df_price: '', //单价
            documentprocess_cc_df_amount: '', //总价
            documentprocess_cc_df_currency: '人民币', //货币
            documentprocess_cc_df_LevyExemption: '照章', //征免
          }
        ]
      },

      //申报要素
      documentprocess_cc_declarationElements: {
        attachedFiles: [], //附件
        documentprocess_cc_de_hscode: '', //海关编码
        documentprocess_cc_de_product: '', //产品
        documentprocess_cc_de_use: '', //用途
        documentprocess_cc_de_brand: '', //品牌
        documentprocess_cc_de_description: '', //型号

        //自定义的字段
        documentprocess_cc_de_def: [
          {
            name: '',
            value: ''
          }, {
            name: '',
            value: ''
          }, {
            name: '',
            value: ''
          }, {
            name: '',
            value: ''
          }, {
            name: '',
            value: ''
          }, {
            name: '',
            value: ''
          }
        ]
      }
    },

    //向工厂索要发票
    documentprocess_askInvoice: {
      attachedFiles: [],

      documentprocess_aski_receiver: '',
      documentprocess_aski_addressCode: '',
      documentprocess_aski_address: '',
      documentprocess_aski_currencyValue: '',

      //发票信息
      documentprocess_aski_table: [
        {
          documentprocess_aski_producuts: '',
          documentprocess_aski_description: '',
          documentprocess_aski_price: '',
          documentprocess_aski_quantity: '',
          documentprocess_aski_unit: '',
          documentprocess_aski_lineAmount: ''
        }
      ]
    },

    //核销资料收集
    documentprocess_collectionData: {
      attachedFiles: []
    }
  },
  actions,
  getters,
  mutations
}

export default cdStore
