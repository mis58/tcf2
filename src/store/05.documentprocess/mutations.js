import * as Types from './mutations-types.js'


let  defaultState = {}, lastDCCData = {};

export const mutations = {
  [Types.SAVE_FORM] (state, value) {
    let temp = _.cloneDeep(value);
    for (var prop in temp) {
      state[prop] = temp[prop];
    }
  },

  //保存初始的state
  [Types.SAVE_INITIAL_DATA] (state, value){
    let temp = _.cloneDeep(state);
    for (var prop in temp) {
      defaultState[prop] = temp[prop];
    }

    // console.log("第一次保存的 defaultState = ", defaultState);
  },

  //重置操作
  [Types.RESET_INITIAL_DATA] (state, value){

    // console.log("重置时的 defaultState = ", defaultState);

    let temp = _.cloneDeep(defaultState);
    for (var prop in temp) {
      state[prop] = temp[prop];
    }
    // console.log("mutations中重置完成后的值：", state);
  },
  [Types.SET_SAVE](state, value){
    state.hasSaved = true;
  },

}

export default mutations
