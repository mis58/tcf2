import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const cdStore = {
  namespaced: true,
  state: {

    hasSaved: false,
    attachedFiles: [],
    relationFormInstId: [], //关联订单

    buyertotcf_bindFContracts: [], //绑定的订单编号
    buyertotcf_bindDContracts: [], //绑定的内贸合同,

    bindingFormInstId: '', //绑定的订单的id

    buyerName: '',
    orderCode: '',

    isSalesMan: true,
    'newApprovalName': '',

    //该流程中的所有审批项
    approvalNames: [
      "业务领导审批定金收汇通知单", "财务审批定金收汇通知单"
    ],

    newApproval: {
      "approval_opinion": "",
      "approval_username": "",
      "approval_time": "",
      "approval_div_name": ""
    },

    prevApprovals: [],

    remainApprovals: [],

    //该流程中的所有审批项
    "originApprovals": [
      {
        "approval_opinion": "",
        "approval_username": "",
        "approval_time": "",
        "approval_div_name": "业务领导审批定金收汇通知单",
        "rw" :  'R',
        item: 'item_02_01'
      }, {
        "approval_opinion": "",
        "approval_username": "",
        "approval_time": "",
        "approval_div_name": "财务审批定金收汇通知单",
        "rw" :  'R',
        item: 'item_02_02'
      }
    ]
  },
  actions,
  getters,
  mutations
}

export default cdStore
