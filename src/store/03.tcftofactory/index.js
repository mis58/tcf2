import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const cdStore = {
  namespaced: true,
  state: {
    hasSaved: false,
    attachedFiles: [],
    relationFormInstId: [],//关联订单
    tcftofactory_bindFContracts: [], //绑定的外贸合同
    tcftofactory_bindDContracts: [],//绑定的内贸合同

    bindingFormInstId: '',// 绑定的外贸合同的id
    isSalesMan: true,

    buyerName: '',
    orderCode: '',

    'newApprovalName': '',

    //该流程中的所有审批项
    approvalNames: [
      "业务领导审批付款申请单", "财务审批付款申请单"
    ],

    newApproval: {
      "approval_opinion": "",
      "approval_username": "",
      "approval_time": "",
      "approval_div_name": ""
    },

    prevApprovals: [],

    remainApprovals: [],

    //该流程中的所有审批项
    "originApprovals": [
      {
        "approval_opinion": "",
        "approval_username": "",
        "approval_time": "",
        "approval_div_name": "业务领导审批付款申请单",
        "rw": 'R',
        item: 'item_03_01'
      }, {
        "approval_opinion": "",
        "approval_username": "",
        "approval_time": "",
        "approval_div_name": "财务审批付款申请单",
        "rw": 'R',
        item: 'item_03_02'
      }
    ],

    tcftofactory_checkedPayMethods: [],
    tcftofactory_thisPaymentTypes: [],
    tcftofactory_nonPaymentTypes: [],
    tcftofactory_applicant: '',
    tcftofactory_applicationDate: '',
    tcftofactory_applicationDepart: '',

    tcftofactory_payCompany: '',
    tcftofactory_payBank: '',
    tcftofactory_payAccount: '',

    tcftofactory_collectionCompany: '',
    tcftofactory_collectionBank: '',
    tcftofactory_collectionAccount: '',

    tcftofactory_paymentAmount: 0,
    tcftofactory_paymentBigAmount: 0,
    tcftofactory_paymentAbstract: '',
    tcftofactory_businessType: '垫税',
    tcftofactory_signDate: '',
    tcftofactory_contractAmount: 0,
    tcftofactory_payedMount: 0,
    tcftofactory_thisPaymentAmount: 0,
    tcftofactory_nonPaymentAmount: 0,
    tcftofactory_dtcNO: ''
  },
  actions,
  getters,
  mutations
}

export default cdStore
