import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const cdStore = {
    state: {
        // merId: '',
    },
    actions,
    getters,
    mutations
}

export default cdStore
