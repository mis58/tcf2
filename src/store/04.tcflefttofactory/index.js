import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const cdStore = {
  namespaced: true,
  state: {
    hasSaved: false,
    relationFormInstId: [], //关联订单

    "isSalesMan": true,

    'newApprovalName': '',


    "approvalNames": [
      "业务领导审批尾款付款申请单",
      "风控审批尾款付款申请单",
      "财务审批尾款付款申请单"
    ],

    newApproval: {
      "approval_opinion":"",
      "approval_username":"",
      "approval_time":"",
      "approval_div_name": ""
    },

    prevApprovals: [],
    remainApprovals: [],

    //该流程中的所有审批项
    "originApprovals":[{
      "approval_opinion":"",
      "approval_username":"",
      "approval_time":"",
      "approval_div_name":"业务领导审批尾款付款申请单",
      "rw": 'R',
      item: 'item_04_01',
    }, {
      "approval_opinion":"",
      "approval_username":"",
      "approval_time":"",
      "approval_div_name":"风控审批尾款付款申请单",
      "rw": 'R',
      item: 'item_04_02',
    }, {
      "approval_opinion":"",
      "approval_username":"",
      "approval_time":"",
      "approval_div_name":"财务审批尾款付款申请单",
      "rw": 'R',
      item: 'item_04_03',
    }],


    tcflefttofactory_qualityassurance: {
      attachedFiles: [],
      tq_bindFContracts: [], //绑定的外贸合同
      tq_bindDContracts: [], //绑定的内贸合同
      selDtc: {},
      'buyerName': '',
      'orderCode': '',

      bindingFormInstId: '',
      
      tq_contractno: '',
      tq_date: '',
      tq_buyer: '',
      tq_address: '',
      tq_tel: '',
      tq_table1: [
        {
          tq_table1_products: '',
          tq_table1_description: '',
          tq_table1_quantity: 0
        }, {
          tq_table1_products: '',
          tq_table1_description: '',
          tq_table1_quantity: 0
        }
      ]
    },
    tcflefttofactory_leftapply: {
      attachedFiles: [],
      tl_applicant: '',
      tl_applicationDate: '',
      tl_applicationDepart: '',
      tl_checkedPayMethods: [],

      tl_payCompany: '',
      tl_payBank: '',
      tl_payAccount: '',

      tl_collectionCompany: '',
      tl_collectionBank: '',
      tl_collectionAccount: '',
      tl_paymentAmount: 0,
      tl_paymentBigAmount: 0,
      tl_paymentAbstract: '',
      tl_businessType: '垫税',
      tl_signDate: '',
      tl_contractAmount: 0,
      tl_payedMount: 0,
      tl_thisPaymentTypes: [],
      tl_nonPaymentTypes: [],
      tl_thisPaymentAmount: 0,
      tl_nonPaymentAmount: 0,
      tl_dtcNO: ''
    }

  },
  actions,
  getters,
  mutations
}

export default cdStore
