import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/store'
import Log from '@/logs'



/**
 * 组件
 * home 首页
 * login 登陆
 * initiatingprocess 发起流程
 * backlog 待办事项
 * customercredit
 * customermanagement
 * dueout 待发事项
 * orderquery 订单查询
 * runmatters 已办事项
 * userManagement 用户管理
 */

const businessForm = ()=> import('@/views/initiatingprocess/01.businessForm/businessForm')
const buyerToTcf  = ()=> import('@/views/initiatingprocess/02.buyerToTCF/buyerToTcf')
const tcfToFactory = ()=>import('@/views/initiatingprocess/03.tcfToFactory/tcfToFactory')
const tcfLeftToFactory = ()=>import('@/views/initiatingprocess/04.tcfLeftToFactory/tcfLeftToFactory')
const documentProcess = ()=>import('@/views/initiatingprocess/05.documentProcess/documentProcess');
const buyerLeftToTcf = ()=>import('@/views/initiatingprocess/06.buyerLeftToTCF/buyerLeftToTcf');

const login = resolve => require(['@/views/login'], resolve)
const home = resolve => require(['@/views/home'], resolve)
const personmsg = resolve => require(['@/views/personmsg'], resolve)
const initiatingprocess = resolve => require(['@/views/initiatingprocess/initiatingprocess'], resolve)
const backlog = resolve => require(['@/views/backlog/backlog'], resolve)
const customercredit = resolve => require(['@/views/customercredit/customercredit'], resolve)
const customermanagement = resolve => require(['@/views/customermanagement/customermanagement'], resolve)
const dueout = resolve => require(['@/views/dueout/dueout'], resolve)
const orderquery = resolve => require(['@/views/orderquery/orderquery'], resolve)
const runmatters = resolve => require(['@/views/runmatters/runmatters'], resolve)
const userManagement = resolve => require(['@/views/userManagement'], resolve)

Vue.use(Router)

/*
 * nav字段
 {
    "item_01": "发起流程",
    "item_02": "待发事项",
    "item_03": "待办事项",
    "item_04": "已办事项",
    "item_05": "订单查询",
    "item_06": "客户管理",
    "item_07": "客户授信",
    "item_08": "用户管理",
    "item_09": "企业查询"
  }
 *
 */

const routes = [
  {
    path: '*',
    redirect: '/home'
  },
  {
    path: '/login',
    name: 'login',
    component: login
  },
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'home',
    component: home
  },
  {
    path: '/personmsg',
    name: 'personmsg',
    component: personmsg
  },
  {
    path: '/initiatingprocess',
    name: 'initiatingprocess',
    component: initiatingprocess,
    children: [
      {
        path: '/initiatingprocess/businessform',
        component: businessForm,
        props: {formIdStart: 'businessform'}
      },{
        path: 'buyertotcf',
        component: buyerToTcf,
        props: {formIdStart: 'buyertotcf'}
      },{
        path: 'tcftofactory',
        component: tcfToFactory,
        props: {formIdStart: 'tcftofactory'}
      },{
        path: 'tcflefttofactory',
        component: tcfLeftToFactory,
        props: {formIdStart: 'tcflefttofactory'}
      },{
        path: 'documentprocess',
        component: documentProcess,
        props: {formIdStart: 'documentprocess'}
      },{
        path: 'buyerlefttotcf',
        component: buyerLeftToTcf,
        props: {formIdStart: 'buyerlefttotcf'}
      }
    ]
  },
  {
    path: '/backlog',
    name: 'backlog',
    component: backlog
  },
  {
    path: '/customercredit',
    name: 'customercredit',
    component: customercredit
  },
  {
    path: '/customermanagement',
    name: 'customermanagement',
    component: customermanagement
  },
  {
    path: '/dueout',
    name: 'dueout',
    component: dueout
  },
  {
    path: '/orderquery',
    name: 'orderquery',
    component: orderquery
  },
  {
    path: '/runmatters',
    name: 'runmatters',
    component: runmatters
  },
  {
    path: '/userManagement',
    name: 'userManagement',
    component: userManagement
  },
  {
    path: '/customer',
    name: 'customer',
    component: () => import('@/views/customer')
  }
]

const router  =  new Router({
  mode: 'history',
  routes
})

router.beforeEach((to, from, next) => {
  store.dispatch("onLoading",true);
  next()
})

router.afterEach((to, from) => {

  Array.prototype.forEach.call(document.getElementsByTagName('*'), (e) => {
    e.blur();
  });

	setTimeout(function(){
  	store.dispatch("onLoading",false);
    //pv 统计
    // Log.init(to.path)
  },300)
})

export default router
