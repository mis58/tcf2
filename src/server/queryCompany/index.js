import {baseUrl} from '../config'
import axios from 'axios'

export const getBaseInfo = (data) =>{
  // {name: xxx}
  return axios.get(`${baseUrl}/cusinfo/baseinfo`, {params: data}, {isJSON: true});
}

export const getReportUrl = (data) => {
  //{keyno: xxx}
  return axios.get(`${baseUrl}/cusinfo/getReport`, {params: data}, {isJSON: true});
}
