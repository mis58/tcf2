import {baseUrl} from '../config.js'
import axios from 'axios'

// 创建表格
export const getMessage = (obj) => {
    return axios.post(`${baseUrl}/customer/query`, obj);
}

// 授信确认
export const creditY = (obj) => {
    return axios.post(`${baseUrl}/customer/save`, obj);
}

// 获取当前行详细信息时获取当前行曾经上传的文件
export const getUpdata = (obj) => {
    return axios.post(`${baseUrl}/common/query`, obj);
}

// 获取查看详情
export const lookMore = (obj) => {
    return axios.get('../../../static/json/lookMore.json', obj)
}

// 保存查看详情更改
export const saveLookMore = (obj) => {
    return axios.get('../../../static/json/lookMore.json', obj)
}
