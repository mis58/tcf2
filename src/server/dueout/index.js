import {baseUrl} from '../config.js'
import axios from 'axios'

//获取表格
export const getTable = (obj) => {
    return axios.post(`${baseUrl}/flow/formInst/query`, obj)
}

//发送
export const commit = (obj) => {
    return axios.get('./../../../static/json/dueoutTable.json', obj)
}

//获取当前发送样式
export const getSendSytle = (obj) => {
    return axios.post(`${baseUrl}/flow/flowNode/nextNode`, obj);
}
