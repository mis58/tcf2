import {testUrl, baseUrl} from '../config'
import axios from 'axios'

/**
 * 部门组织架构
 */
export const getOrgQuery = (obj) => {
   return axios.post(`${baseUrl}/user/org/query`, obj);
}

/**
 * 新增组织架构
 */
export const getSaveQuery = (obj) => {
   return axios.post(`${baseUrl}/user/org/save`, obj);
}

/**
 * 查询用户列表
 */
export const userQuery = (obj) => {
   return axios.post(`${baseUrl}/user/query`, obj);
}

/**
 * 重置密码
 */
export const resetPassWord = (obj) => {
   return axios.post(`${baseUrl}/user/resetPassWord`, obj);
}

/**
 * 新增用户
 */
export const addUser = (obj) => {
   return axios.post(`${baseUrl}/user/createUser`, obj);
}

/**
 * 当前用户信息
 */
export const userMsg = (obj) => {
   return axios.post(`${baseUrl}/user/roleUser/query`, obj);
}

/**
 * 编辑用户-保存
 */
export const editUser = (obj) => {
   return axios.post(`${baseUrl}/user/modifyUser`, obj);
}

/**
 * 删除用户
 */
export const delUser = (obj) => {
   return axios.post(`${baseUrl}/user/delete`, obj);
}

/**
 * 角色列表
 */
export const roleQuery = (obj) => {
   return axios.post(`${baseUrl}/user/roleUser/query`, obj);
}

/**
 * 已分配用户
 */
export const getBindings = (obj) => {
   return axios.post(`${baseUrl}/user/role/getBindings`, obj);
}

/**
 * 待分配用户
 */
export const getUnBindings = (obj) => {
   return axios.post(`${baseUrl}/user/role/getUnBindings`, obj);
}

/**
 * 确认分配用户
 */
export const bindUsers = (obj) => {
   return axios.post(`${baseUrl}/user/role/bindUsers`, obj);
}
