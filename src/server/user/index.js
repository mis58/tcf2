import {testUrl, baseUrl} from '../config'
import axios from 'axios'

export const getInfo = () => {
   return axios.get(`${baseUrl}/user/info`);
}

export const userLogin = (obj) => {
   return axios.post(`${baseUrl}/user/login`, obj);
}

export const logout = () => {
   return axios.post(`${baseUrl}/user/logout`);
}
