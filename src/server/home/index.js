import {testUrl, baseUrl} from '../config'
import axios from 'axios'

/**
 * 获取待办／已办list
 */
// export const getItemData = (obj) => {
//    return axios.get(`${testUrl}/home/getItem`, obj);
// }

export const todoList = (obj) => {
   return axios.post(`${baseUrl}/flow/formInst/todoList`, obj);
}

export const hivedoList = (obj) => {
   return axios.post(`${baseUrl}/flow/hiveToDo/query`, obj);
}

// export const getItemData = (obj) => {
//   //  return axios.post(`${baseUrl}/home/getItem`, obj);
//    return axios.post(`${baseUrl}/system/menu/getAll.ajax/1/cdpm`, obj);
//
// }
