import {testUrl, baseUrl} from '../config.js'
import axios from 'axios'

//进入 业务提单流程时 获得 订单编号等
export const getOrderNumber = (arg) => {
  /*
  common/sn/nextSn?year=2018
  */
  return axios.get(`${baseUrl}/common/sn/nextSn`, {params: arg}, {
    isJSON: true
  });
}



//获得买方，卖方的信息
export const  getBusinessMesg = (arg)=> {
  // {
  //   type: 1
  // }
  return axios.get(`${baseUrl}/customer/query`, {params: arg}, {isJSON: true});
}

//票据附件的上传
export const uploadFiles = (files) => {

  var promiseAll = [];
  for (let i = 0; i < files.length; i++) {
    let formdata = new FormData();
    formdata.append('file', files[i], files[i].name);

    promiseAll.push(axios.post(`${baseUrl}/common/upload`, formdata, {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      isJSON: true
    }))
  }

  return axios.all(promiseAll);
}

// formInstId=2&formServiceName=business_lading`
//从服务器端获得数据
export const getData = (arg) => {
  return axios.get(`${baseUrl}/flow/formInst/getFormInstData`, {
    params: {
      ...arg
    }
  });
}

// /flow/formInst/getPermissionByNodeId?flowId=
export const getPermission = (arg) => {
  return axios.get(`${baseUrl}/flow/formInst/getPermission`, {
    params: {
      ...arg
    }
  })
}


//导出功能
export const exportFile = (arg) => {
  return axios.get(`${baseUrl}/xxx`, {});
}

//导出Excel
export const exportExcel = (arg) => {
  return axios.get(`${baseUrl}/flow/formInst/exportExcel`, {
    params: {
      ...arg
    }
  })
}
//下载Excel的url
export const downloadExcelUrl = `${baseUrl}/flow/formInst/downloadExcel`

//绑定订单时,查询符合条件的外贸订单
export const queryBindContracts = (arg) => {
  return axios.get(`${baseUrl}/flow/formInst/query`,{params: arg}, {
    isJSON: true
  });
}

//绑定订单时,查询符合条件的 外贸对应内贸订单
export const queryBindDtcContracts = (arg) => {
  return axios.get(`${baseUrl}/flow/dtc/query`,{params: arg}, {
    isJSON: true
  });
}


//查找外贸的具体数据
// flow/ftc/query?formInstId=26

export const queryFtcContract = (arg) => {
  return axios.get(`${baseUrl}/flow/ftc/query`,{params: arg}, {
    isJSON: true
  });
}

//查找外贸中的table表格数据
export const queryFtcContractTable = (arg) => {
  /*
  {
  formInstId: xxx,
  ftcId: '',
}
  */
  return axios.get(`${baseUrl}/flow/ftcpd/query`,{params: arg}, {
    isJSON: true
  });
}


//关联订单时，查询符合条件的外贸订单
export const queryForeignContracts = (arg) => {
  return axios.get(`${baseUrl}/xxx`, arg);
}

//保存到服务器
export const saveData = (arg) => {
  return axios.post(`${baseUrl}/flow/formInst/temporary`, arg, {
    headers: {
      'Content-Type': 'application/json'
    },
    isJSON: true
  });
}

//获得发送按钮的选项
export const getSendMenus = (arg)=>{
  // flowId=1
  return axios.get(`${baseUrl}/flow/flowNode/nextNodes`, {params: arg}, {
    isJSON: true
  });
}

//发送按钮
export const sendNext = (arg) => {
  return axios.post(`${baseUrl}/flow/formInst/send`, arg, {
    headers: {
      'Content-Type': 'application/json'
    },
    isJSON: true
  });
}
