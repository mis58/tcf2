import axios from 'axios'
import Qs from 'qs'

/**
 * 代理url
 */
let hostname = location.hostname;

export const testUrl = '/fe'
export const baseUrl = '/api'
export const loginUrl = '/'

/**
 * 中间件扩展catch
 */

// axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'
// axios.defaults.headers['Content-Type'] = 'application/json;charset=UTF-8'

//请求前
// 通过config的isJSON判断 数据格式
axios.interceptors.request.use((config) => {
  // let sData = JSON.parse(JSON.stringify(data), function (k, v) {
  //   if (!(v === '')) return v;
  // });
  if (!config.isJSON) {
    config.transformRequest = (data) => {
      //处理序列化参数  form-data 格式
      // var sData = JSON.parse(JSON.stringify(data), function (k, v) {
      //   if (!(v === '')) return v;
      // });

      data = Qs.stringify(data);
      return data

    }
  }
  config.withCredentials = true
  return config
}, (error) => {
  return Promise.reject(error)
});

//请求后
axios.interceptors.response.use((response) => {
  // if (response.data.do == "redirect") {
  // window.location.href = response.data.url
  // }else{]
  if (response.status == '200') {
    return response.data;
  }
}, (error) => {
    let errMark = document.getElementById('errMark');
    errMark.style.display = 'block';
    setTimeout(()=> {
      errMark.style.display = 'none';
    },3000)
    return Promise.reject(error)
});
