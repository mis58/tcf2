import {testUrl, baseUrl} from '../config'
import axios from 'axios'

/**
 * tree
 */

// const GETSUBBUS = `${testUrl}/user/org/getSubBus`
// const GETROOTBUS = `${testUrl}/user/org/getRootBus`
// const ADDBUS = `${testUrl}/user/org/addBus`
// const UPDATEBUSNAME = `${testUrl}/user/org/updateBusName`
// const UPDATEBUSORDER = `${testUrl}/user/org/updateBusOrder`
// const DELBUS = `${testUrl}/user/org/delBus`

const GETROOTBUS = `${baseUrl}/user/org/query?pid=-1&status=1`
const GETSUBBUS = `${baseUrl}/user/org/query`
const ADDBUS = `${baseUrl}/user/org/save`
const UPDATEBUSNAME = `${baseUrl}/user/org/save`
const UPDATEBUSORDER = `${baseUrl}/user/org/updateBusOrder`
const DELBUS = `${baseUrl}/user/org/save`

const isSuccess = (resp) => {
  return resp.code && resp.code === 1
}

const basicSuccess = (resp) => {
  if (isSuccess(resp)) {
    try {
      return resp.data.datas
    } catch (e) {
      return resp.data
    }

  } else {
    Message(resp.message)
    return resp.code
  }
}
const basic = (url, method) => {
  return (params) => {
    return axios[method](url, params).then(basicSuccess)
  }
}

export default {
  getSubBus: basic(GETSUBBUS, 'post'),
  getRootBus: basic(GETROOTBUS, 'post'),
  addBus: basic(ADDBUS, 'post'),
  updateBusName: basic(UPDATEBUSNAME, 'post'),
  updateBusOrder: basic(UPDATEBUSORDER, 'post'),
  delBus: basic(DELBUS, 'post')
}
