import {testUrl, baseUrl} from '../config'
import axios from 'axios'

const baseBath = baseUrl + '/newCustomer'

export default {

  /**
   * 分页查询客户列表
   */
  page: (params) => axios.get(`${baseBath}/query`, { params }),
  /**
   * 根据ID查询客户
   */
  get: (id) => axios.get(`${baseBath}/query`, { params : { id }}),
  /**
   * 保存
   * @param data
   */
  save: (data) => axios.post(`${baseBath}`, data, {
    headers: {
      'Content-Type': 'application/json'
    },
    isJSON: true
  }),
  /**
   * 修改
   * @param id
   */
  delete: (id) => axios.delete(`${baseBath}/${id}`),

  uploadUrl: `${baseBath}/upload`
}