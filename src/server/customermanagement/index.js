import {baseUrl} from '../config.js'
import axios from 'axios'

// 获取表格
export const getMessage = (obj) => {
    return axios.post(`${baseUrl}/customer/query`, obj);
}

// 上传新增买家
export const addNew = (obj) => {
    return axios.post(`${baseUrl}/customer/save`, obj);
}


// 确认删除表格内容
export const deleteSure = (obj) => {
    return axios.post(`${baseUrl}/customer/delete`, obj);
}

// 获取当前行详细信息时获取当前行曾经上传的文件
export const getUpdata = (obj) => {
    return axios.post(`${baseUrl}/common/query`, obj);
}

// 上传
export const actionList = (obj) => {
    return axios.post(`${baseUrl}/common/upload`, obj);
}

// 下载
export const filedownload = (obj) => {
    return axios.post(`${baseUrl}/common/download`, obj);
}

// 删除附件
export const deleteFile = (obj) => {
    return axios.post(`${baseUrl}/common/delete`, obj);
}
