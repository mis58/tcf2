import {baseUrl} from '../config'
import axios from 'axios'

/**
 * 获取基本信息
 */
export const getInfo = (obj) => {
   return axios.get(`${baseUrl}/user/info`, obj);
}

/**
 * 修改基本信息
 */
export const modifyUserInfo = (obj) => {
   return axios.post(`${baseUrl}/user/modifyUserInfo`, obj);
}

/**
 * 修改密码
 */
export const modifyPassWord = (obj) => {
   return axios.post(`${baseUrl}/user/modifyPassWord`, obj);
}
