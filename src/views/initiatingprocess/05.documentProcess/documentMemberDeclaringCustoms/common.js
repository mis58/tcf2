
/**
 * 处理 合同的通知和校验  
 * @param {Function} checkFn 校验函数
 * @param {Function} callback 回调方法 
 */
export const  handleEmit  =  (checkFn, callback) => {
    if (checkFn && (typeof checkFn === 'function')) {
        checkFn();
        return;
    }
    
    callback && callback();
}